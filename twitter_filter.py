import os

import tweepy


def getTweets(query):
    client = tweepy.Client(bearer_token=os.getenv('TOKEN'))

    tweets = client.search_recent_tweets(query=query, tweet_fields=['context_annotations', 'created_at'],
                                         max_results=10)
    return tweets.data

