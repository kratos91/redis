from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer


def getAnalysis(tweet):
    blob_object = TextBlob(tweet.text, analyzer=NaiveBayesAnalyzer())
    analysis = blob_object.sentiment
    return analysis
