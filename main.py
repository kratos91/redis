import os
from twitter_filter import getTweets
from analysis import getAnalysis
from storage import TweetStore

tweets = getTweets('apple OR macbook lang:en')
s = TweetStore()

for tweet in tweets:
    t = getAnalysis(tweet)
    dictT = {'tweeet': tweet.text, 'classification': t.classification, 'val_pos': t.p_pos, 'val_neg': t.p_neg}
    s.push(dictT)

print("Se insertaron los valores")
r = s.tweets(10)
print(r)
